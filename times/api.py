# Brevet times service

import os
import flask
from flask import Flask, request, abort
from flask_restful import Resource, Api
from flask_httpauth import HTTPTokenAuth
import pymongo
from pymongo import MongoClient
from passlib.apps import custom_app_context as pwd_context

from itsdangerous import (TimedJSONWebSignatureSerializer \
                                  as Serializer, BadSignature, \
                                  SignatureExpired)
# Instantiate the app
app = Flask(__name__)
api = Api(app)
auth = HTTPTokenAuth()
client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.controls

# Define resources

class All(Resource):
	@auth.login_required
	def get(self):
		top = request.args.get("top")

		items = [item for item in db.rows.find().sort("km", pymongo.ASCENDING)]

		if top != None:
			items = items[:top]
		return {'open': [item['open'] for item in items], 'close': [item['close'] for item in items]}

class OpenOnly(Resource):
	@auth.login_required
	def get(self):
		top = request.args.get("top")

		items = [item for item in db.rows.find().sort("open", pymongo.ASCENDING)]

		if top != None:
			items = items[:top]
		return {'open': [item['open'] for item in items]}

class CloseOnly(Resource):
	@auth.login_required
	def get(self):
		top = request.args.get("top")

		items = [item for item in db.rows.find().sort("close", pymongo.ASCENDING)]

		if top != None:
			items = items[:top]
		return {'close': [item['close'] for item in items]}

class AllFormatted(Resource):
	@auth.login_required
	def get(self, format):
		top = request.args.get("top")

		items = [item for item in db.rows.find().sort("km", pymongo.ASCENDING)]

		if top != None:
			items = items[:top]

		if format == 'json':
			return flask.jsonify({'open': [item['open'] for item in items], 'close': [item['close'] for item in items]})
		elif format == 'csv':
			data = []
			for item in items:
				data += str(item['open']) + ", " + str(item['close']) + ", "
			return data

class OpenOnlyFormatted(Resource):
	@auth.login_required
	def get(self, format):
		top = request.args.get("top")

		items = [item for item in db.rows.find().sort("open", pymongo.ASCENDING)]

		if top != None:
			items = items[:top]

		if format == 'json':
			return flask.jsonify({'open': [item['open'] for item in items]})
		elif format == 'csv':
			data = []
			for item in items:
				data += item['open'] + ", "
			return data

class CloseOnlyFormatted(Resource):
	@auth.login_required
	def get(self, format):
		top = request.args.get("top")

		items = [item for item in db.rows.find().sort("close", pymongo.ASCENDING)]

		if top != None:
			items = items[:top]

		if format == 'json':
			return flask.jsonify({'close': [item['close'] for item in items]})
		elif format == 'csv':
			data = []
			for item in items:
				data += item['close'] + ", "
			return data

# Create routes
# Another way, without decorators
api.add_resource(All, '/listAll')
api.add_resource(OpenOnly, '/listOpenOnly')
api.add_resource(CloseOnly, '/listCloseOnly')

api.add_resource(AllFormatted, '/listAll/<string:format>/')
api.add_resource(OpenOnlyFormatted, '/listOpenOnly/<string:format>/')
api.add_resource(CloseOnlyFormatted, '/listCloseOnly/<string:format>/')

@app.route("/api/register", methods=['POST'])
def register():
	new_user_fields = request.form

	username = new_user_fields['username']
	password = hash_password(new_user_fields['password'])

	if db.users.find_one({'username':{ '$eq': username}}) is not None:
		# usernames must be unique
		abort(400)

	user_id = str(db.users.insert({'username': username, 'password': password}))
	return flask.jsonify({'id': user_id, 'username': username})

@app.route("/api/token")
def issue_token():
	auth = request.authorization
	if auth['username'] is None or auth['password'] is None:
		abort(401)

	user_data = db.users.find_one({'username':{ '$eq': auth['username']}})
	if user_data is None:
		abort(401)

	if verify_password(auth['password'], user_data['password']):
		return generate_auth_token(str(user_data['_id']))

	abort(401)
@auth.verify_token
def authenticate_with_token(token):
	if verify_auth_token(token) is None:
		return False
	return True
# hash a password for storing in the database
def hash_password(password):
    return pwd_context.encrypt(password)

def verify_password(password, hashVal):
    return pwd_context.verify(password, hashVal)

def generate_auth_token(user_id, expiration=600):
   # s = Serializer(app.config['SECRET_KEY'], expires_in=expiration)
   s = Serializer('test1234@#$', expires_in=expiration)
   # pass index of user
   return s.dumps({'id': user_id})

def verify_auth_token(token):
    s = Serializer('test1234@#$')
    try:
        data = s.loads(token)
    except SignatureExpired:
        return None    # valid token, but expired
    except BadSignature:
        return None    # invalid token
    return data['id']
# Run the application
if __name__ == '__main__':
	app.run(host='0.0.0.0', port=5001, debug=True)
